package application;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.xml.ws.Response;

import application.jdownloader.JDReader;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableListValue;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.CheckBoxTreeItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.control.cell.CheckBoxTreeCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.HBoxBuilder;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.util.Callback;

public class MainUIController implements Initializable{
	
	private ObservableList<FileInformation> fiData;
	private CheckBoxTreeItem<File> rootItem = null;
//	private Map<CheckBoxTreeItem<String>, File> allFoldersMap = new HashMap<>();
	private List<CheckBoxTreeItem<File>> allTreeItemsList = new ArrayList<>();
	
	//--- Settings
	
	@FXML
	private CheckBox cbName;
	
	@FXML
	private CheckBox cbHash;
	
	@FXML
	private CheckBox cbSize;
	
	@FXML
	private CheckBox cbDuration;
	
	@FXML
	private CheckBox cbResolution;
	
	@FXML
	private CheckBox cbBitrate;
	
	@FXML
	private CheckBox cbLivePop;
	
	@FXML
	private HBox hbJDPath;
	
	@FXML
	private TextField tfJDPath;
	
	
	//--- Tree Folder
	@FXML
	private TreeView<File> treeFolder;
	
	@FXML
	private TableView<DublicateFileInfo> listDubFiles;
		
	@FXML
	private TableColumn<DublicateFileInfo, String> colDubNames;
		
	@FXML
	private TableColumn<DublicateFileInfo, Integer> colDubCount;
	
	@FXML
	private TableColumn<DublicateFileInfo, String> colNameBool;
	
	@FXML
	private TableColumn<DublicateFileInfo, String> colHashBool;
	
	@FXML
	private TableColumn<DublicateFileInfo, String> colSizeBool;
	
	@FXML
	private TableColumn<DublicateFileInfo, String> colDurationBool;
	
	@FXML
	private TableColumn<DublicateFileInfo, String> colBitrateBool;
	
	@FXML
	private TableColumn<DublicateFileInfo, String> colResolutionBool;
	
	//--- Info
	@FXML
	private TableView<FileInformation> listInfoFiles;
		
	@FXML
	private TableColumn<FileInformation, String> colInfoName;
	
	@FXML
	private TableColumn<FileInformation, String> colInfoDir;
	
	@FXML
	private TableColumn<FileInformation, Long> colInfoSize;
	
	@FXML
	private TableColumn<FileInformation, Long> colInfoDuration;
	
	@FXML
	private TableColumn<FileInformation, Integer> colInfoBitrate;
	
	@FXML
	private TableColumn<FileInformation, String> colInfoResolution;
	
	@FXML
	private TableColumn<FileInformation, String> colInfoHash;
	

	@FXML
	void actionResetFolder(ActionEvent event) {
		if(rootItem != null /*&& rootItem.getChildren() != null || rootItem.getChildren().size() > 0*/){
			rootItem.getChildren().removeAll(allTreeItemsList);
			allTreeItemsList = new ArrayList<>();
		}
	}

	@FXML
	void actionAddFolder(ActionEvent event) {
		//file chooser
		DirectoryChooser chooser = new DirectoryChooser();
		chooser.setTitle("Choose File");
		File selectedDirectory = chooser.showDialog(Main.getPrimaryStage());
		if(selectedDirectory == null) return;
		addFolder(selectedDirectory);
	}
	
	private void addFolder(File f){
		List<File> folders = new ArrayList<>();
		for(int i = 0; i < f.list().length; i++){
			if(f.listFiles()[i].isDirectory()){
				folders.add(f.listFiles()[i]);
			}else{
//				FileAnalyser.addFile(selectedDirectory.listFiles()[i]);
			}
		}
		
		if(rootItem == null){
			rootItem = new CheckBoxTreeItem<File>(new File("scan selected folder"));
			rootItem.setExpanded(true);
			rootItem.setSelected(true);
			
			treeFolder.setRoot(rootItem);
			treeFolder.setEditable(true);
			treeFolder.setShowRoot(true);
		}
		
		final CheckBoxTreeItem<File> checkBoxTreeItem = 
				new CheckBoxTreeItem<File>(f);
		checkBoxTreeItem.setSelected(true);
		allTreeItemsList.add(checkBoxTreeItem);
		rootItem.getChildren().add(checkBoxTreeItem);

		treeFolder.setCellFactory(CheckBoxTreeCell.<File>forTreeView());
	}
	
	@FXML
	void actionScanFolders(ActionEvent event){
		
		//scan treeFolder for selected items
		List<File> allFolders = new ArrayList<>();
		for(CheckBoxTreeItem<File> ti : allTreeItemsList){
			if(ti.isSelected()){
				allFolders.add(ti.getValue());
				FileAnalyser.addFolder2List(ti.getValue());
			}
		}
		
		FileAnalyser.scanFolders(allFolders); //start scan
		List<DublicateFileInfo> dublicates = FileAnalyser.getDublicates();
		final ObservableList<DublicateFileInfo> dfiData = FXCollections.observableArrayList(dublicates);
		
		colDubNames.setCellValueFactory(new PropertyValueFactory<DublicateFileInfo, String>("name"));
//		if(colDubCount == null) colDubCount = new TableColumn<>("Counter");
		colDubCount.setCellValueFactory(new PropertyValueFactory<DublicateFileInfo, Integer>("count"));
		colNameBool.setCellValueFactory(new PropertyValueFactory<DublicateFileInfo, String>("isDubName"));
		colHashBool.setCellValueFactory(new PropertyValueFactory<DublicateFileInfo, String>("isDubHash"));
		colSizeBool.setCellValueFactory(new PropertyValueFactory<DublicateFileInfo, String>("isDubSize"));
//		if(colDurationBool == null) colDurationBool = new TableColumn<>("Duration");
		colDurationBool.setCellValueFactory(new PropertyValueFactory<DublicateFileInfo, String>("isDubDuration"));
		colBitrateBool.setCellValueFactory(new PropertyValueFactory<DublicateFileInfo, String>("isDubBitrate"));
		colResolutionBool.setCellValueFactory(new PropertyValueFactory<DublicateFileInfo, String>("isDubResolution"));
		
		listDubFiles.setItems(dfiData);
		
		listDubFiles.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Object>() {
			@Override
			public void changed(ObservableValue<?> observableValue, Object oldValue, Object newValue) {
				//Check whether item is selected and set value of selected item to Label
				if(listDubFiles.getSelectionModel().getSelectedItem() != null) {
					DublicateFileInfo dfi = (DublicateFileInfo) listDubFiles.getSelectionModel().getSelectedItem();
					System.out.println(dfi.getName());
					fiData = FXCollections.observableArrayList(dfi.getFileInfos());
					colInfoName.setCellValueFactory(new PropertyValueFactory<FileInformation, String>("name"));
					colInfoDir.setCellValueFactory(new PropertyValueFactory<FileInformation, String>("path"));
					colInfoSize.setCellValueFactory(new PropertyValueFactory<FileInformation, Long>("size"));
//					if(colInfoDuration == null) colInfoDuration = new TableColumn<>("Duration");
					colInfoDuration.setCellValueFactory( new PropertyValueFactory<FileInformation, Long>("duration"));
//					if(colInfoBitrate == null) colInfoBitrate = new TableColumn<>("Bitrate");
					colInfoBitrate.setCellValueFactory(new PropertyValueFactory<FileInformation, Integer>("bitrate"));
		//			colInfoSize.setCellValueFactory(new PropertyValueFactory<FileInformation, String>("name"));
					colInfoResolution.setCellValueFactory(new PropertyValueFactory<FileInformation, String>("resolution"));
//					colInfoHeight.setCellValueFactory(new PropertyValueFactory<FileInformation, Integer>("height"));
					listInfoFiles.setItems(fiData);
				}
			}
		});
		
//		colInfoName.get.addEventHandler(new EventHandler<KeyEvent>() {
//			@Override
//			public void handle(KeyEvent event) {
//				if(event.getCode() == KeyCode.DELETE); {
//					System.out.println("delete");
//					event.consume();
//				}
//			};
//		};
		listInfoFiles.getItems().clear();
		listInfoFiles.addEventHandler(KeyEvent.ANY, deleteRow);
		listInfoFiles.addEventHandler(MouseEvent.MOUSE_CLICKED, doubleClick);
//		.getSelectionModel().selectedItemProperty().addListener(new EventHandler<KeyEvent>() {
//			@Override
//			public void handle(KeyEvent event) {
//				if(event.getCode() == KeyCode.DELETE); {
//					System.out.println("delete");
//					event.consume();
//				} 
//			}
//		};
//		EventHandler<KeyEvent>() {
//            @Override
//            public void handle(KeyEvent t) {
//            		if(listDubFiles.getSelectionModel().getSelectedItem() != null) {
//    					FileInformation fi = listInfoFiles.getSelectionModel().getSelectedItem();
//    				}
//            }
//			@Override
//			public void changed(ObservableValue<?> observableValue, Object oldValue, Object newValue) {
//				//Check whether item is selected and set value of selected item to Label
//				
//			}
//		});
	}
	
//	@FXML
//	void eventFileInfoKeyReleased(ActionEvent event) {
//		if(listDubFiles.getSelectionModel().getSelectedItem() != null) {
//			FileInformation fi = listInfoFiles.getSelectionModel().getSelectedItem();
//		}
//	}
	@FXML
	void actionCbName(ActionEvent event){
		colNameBool.setVisible(cbName.isSelected());
		FileAnalyser.setIsDubNameOn(cbName.isSelected());
	}
	@FXML
	void actionCbHash(ActionEvent event){
		colHashBool.setVisible(cbHash.isSelected());
		FileAnalyser.setIsDubHashOn(cbHash.isSelected());
	}
	@FXML
	void actionCbSize(ActionEvent event){
		colSizeBool.setVisible(cbSize.isSelected());
		FileAnalyser.setIsDubSizeOn(cbSize.isSelected());
	}
	@FXML
	void actionCbDuration(ActionEvent event){
		colDurationBool.setVisible(cbDuration.isSelected());
		FileAnalyser.setIsDubDurationOn(cbDuration.isSelected());
	}
	@FXML
	void actionCbBitrate(ActionEvent event){
		colBitrateBool.setVisible(cbBitrate.isSelected());
		FileAnalyser.setIsDubBitrateOn(cbBitrate.isSelected());
	}
	@FXML
	void actionCbResolution(ActionEvent event){
		colResolutionBool.setVisible(cbResolution.isSelected());
		FileAnalyser.setIsDubResolutionOn(cbResolution.isSelected());
	}
	
	
	//--- JDReader Events
	@FXML
	void actionCbLivePop(ActionEvent event){
		
	}
	@FXML
	void actionCbLiveObs(ActionEvent event){
		CheckBox cb = (CheckBox) event.getSource();
		boolean isSelected = cb.isSelected();
		cbLivePop.setDisable(!isSelected);
//		if(hbJDPath == null) hbJDPath = new HBox();
		hbJDPath.setDisable(!isSelected);
	}
	@FXML
	void actionBtnJDPath(ActionEvent event){
		//file chooser
		DirectoryChooser chooser = new DirectoryChooser();
		chooser.setTitle("Choose File");
		File selectedDirectory = chooser.showDialog(Main.getPrimaryStage());
		if(selectedDirectory == null) return;
		tfJDPath.setText(selectedDirectory.getAbsolutePath());
	}
	
	@FXML
	void actionTfJDPath(ActionEvent event) {
		//
	}
	
	@FXML
	void actionBtnStartObs(ActionEvent event) {
		JDReader.start(tfJDPath.getText());
	}
	
	private EventHandler<MouseEvent> doubleClick = new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
			if(event.getClickCount() == 2) {
				if(listDubFiles.getSelectionModel().getSelectedItem() != null) {
					try {
						if(event.isShiftDown()){
							Desktop.getDesktop().open(listInfoFiles.getSelectionModel().getSelectedItem().getFile().getParentFile());
						}else{
							Desktop.getDesktop().open(listInfoFiles.getSelectionModel().getSelectedItem().getFile());
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
//					if(FileAnalyser.deleteFileFromDisc(listInfoFiles.getSelectionModel().getSelectedItem().getFile()))fiData.remove(listInfoFiles.getSelectionModel().getSelectedItem());
				}
				event.consume();
			}
		};
	};
	
	private EventHandler<KeyEvent> deleteRow = new EventHandler<KeyEvent>() {
		@Override
		public void handle(KeyEvent event) {
			if(event.getCode() == KeyCode.DELETE) {
				if(listDubFiles.getSelectionModel().getSelectedItem() != null) {
					if(FileAnalyser.deleteFileFromDisc(listInfoFiles.getSelectionModel().getSelectedItem().getFile()))fiData.remove(listInfoFiles.getSelectionModel().getSelectedItem());
				}
				event.consume();
			}
		};
	};

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		colNameBool.setVisible(cbName.isSelected());
		colHashBool.setVisible(cbHash.isSelected());
		colSizeBool.setVisible(cbSize.isSelected());
		colDurationBool.setVisible(cbDuration.isSelected());
		colResolutionBool.setVisible(cbResolution.isSelected());
		colBitrateBool.setVisible(cbBitrate.isSelected());
		
		if(tfJDPath.getText() == null || tfJDPath.getText().equals("")){
			File f = new File(System.getProperty("user.home") + "/AppData/Local/JDownloader v2.0/");
			if(f.isDirectory()){
				tfJDPath.setText(f.getAbsolutePath());
			}
		}
		
		treeFolder.setOnDragOver(new EventHandler<DragEvent>() {
			@Override
			public void handle(DragEvent event) {
				Dragboard db = event.getDragboard();
				if (db.hasFiles()) {
					event.acceptTransferModes(TransferMode.COPY);
				} else {
					event.consume();
				}
			}
		});
		
		// Dropping over surface
		treeFolder.setOnDragDropped(new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                boolean success = false;
                if (db.hasFiles()) {
                    success = true;
                    for (File file:db.getFiles()) {
//                        filePath = file.getAbsolutePath();
                        addFolder(file);
                    }
                }
                event.setDropCompleted(success);
                event.consume();
            }
        });
	}
}
