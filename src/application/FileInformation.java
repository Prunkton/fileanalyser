package application;

import java.io.File;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Calendar;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.xuggle.xuggler.IContainer;
import com.xuggle.xuggler.IStreamCoder;

//@PersistenceCapable
//@Entity
public class FileInformation {
//	@Id
	private final SimpleStringProperty name;
	private final SimpleStringProperty path;
	private final SimpleStringProperty type;
	private final SimpleLongProperty size;
	private final SimpleStringProperty format;
	private final SimpleDoubleProperty frameRate;
	private final SimpleIntegerProperty width;
	private final SimpleIntegerProperty height;
	private final SimpleStringProperty resolution;
	private final SimpleDoubleProperty bitrate;
	private final SimpleStringProperty hashString;
	private final SimpleDoubleProperty duration;
//	private final SimpleObjectProperty<Calendar> dateCreation;
//	private final SimpleObjectProperty<Calendar> dateLastEdit;

	private File f;
	private byte[] hashByte;
//	private IContainer container;
//	private IStreamCoder coder;
//	f, b, raf.length(), format, bitRate, frameRate, width, height, duration
	public FileInformation(File f, byte[] hash, long size, String format, double bitRate, double frameRate, int width, int height, double duration){
		
		this.size = new SimpleLongProperty(size);
		this.format = new SimpleStringProperty(format);
		this.frameRate = new SimpleDoubleProperty(frameRate);
		this.width = new SimpleIntegerProperty(width);
		this.height = new SimpleIntegerProperty(height);
		this.path = new SimpleStringProperty(f.getAbsolutePath());
		this.name = new SimpleStringProperty(f.getName().substring(0, f.getName().lastIndexOf(".")));
		this.type = new SimpleStringProperty(f.getName().substring(f.getName().lastIndexOf("."), f.getName().length()));
		this.bitrate = new SimpleDoubleProperty(bitRate);
		this.hashString = new SimpleStringProperty(new BigInteger(1, hash).toString(16));
		this.duration = new SimpleDoubleProperty(duration);
		this.resolution = new SimpleStringProperty(height + " x " + width);
		
		this.f = f;
		this.hashByte = hash;
	}
	
	public String getName() {
		return name.get();
	}
	
	public String getPath(){
		return path.get();
	}
	
	public String getType(){
		return type.get();
	}
	
	public Long getSize(){
		return size.get();
	}
	
	public Double getBitrate(){
		return bitrate.get();
	}
	
	public String getHash(){
		return hashString.get();
	}
	
	public Double getDuration(){
		return duration.get();
	}
	
	public Integer getHeight(){
		return height.get();
	}
	
	public Integer getWidth(){
		return width.get();
	}
	
	public File getFile(){
		return f;
	}
	
	public byte[] getHashByte(){
		return hashByte;
	}
	
	public String getResolution(){
		return resolution.get();
	}
	
//	public IContainer getContainer(){
//		return container;
//	}
//	
//	public IStreamCoder getCoder(){
//		return coder;
//	}
	
	@Override
	public String toString(){
		return "Name: " + this.getName() + ", Size: " + this.getSize();
	}
}
