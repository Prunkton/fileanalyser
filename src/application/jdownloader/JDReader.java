package application.jdownloader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import org.apache.commons.io.comparator.LastModifiedFileComparator;

import application.jdownloader.container.Container;
import application.jdownloader.container.Link;
import application.jdownloader.container.Properties;

import com.github.junrar.Archive;
import com.github.junrar.exception.RarException;
import com.github.junrar.impl.FileVolumeManager;
import com.github.junrar.rarfile.FileHeader;

public class JDReader {
	
	private static final String LINKSSNIPPED_START = " : [ {";
	private static final String LINKSSNIPPED_END = "  } ],";
	private static final String NAME = "name";
	private static final String UID = "uid";
	private static final String CREATED = "created";
	private static final String URL = "url";
	private static final String DOWNLOADFOLDER = "downloadFolder";
	private static final String HOST = "host";
	private static final String SIZE = "size";
	private static final String BROWSERURL = "browserURL";
	private static final String COMMA = ",";
	private static final String QUOTE = "\"";
	private static final String COLON = ":";
	
	private static final int LINKSSNIPPED_START_LENGTH = 6;
	private static final int DISTANCE_SIZE_NUMBER = SIZE.length() + 4; //name + extension
	private static final int DISTANCE_NAME_STRING = NAME.length() + 5; //name + extension
	private static final int DISTANCE_UID_STRING = UID.length() + 4;
	private static final int DISTANCE_BROWSERURL_STRING = BROWSERURL.length() + 5;
	private static final int DISTANCE_HOST_STRING = HOST.length() + 5;
	private static final int DISTANCE_URL_STRING = URL.length() + 5;
	private static final int DISTANCE_CREATED_STRING = CREATED.length() + 4;
	private static final int DISTANCE_DOWNLOADFOLDER_STRING = DOWNLOADFOLDER.length() + 5;
	
	
	private static String rootDir;
	private static File srcFolder;
	private static File downloadList;
	
	private static int refreshTime = 10000;
	
	private static InputStream is;
	private static InputStreamReader isr;
	private static ZipFile zf;;
	private static ZipEntry entry;
	private static StringBuilder s;
	private static List<StringBuilder> sList;
	private static char[] buffer;
	
	private static List<Container> containerList = new ArrayList<>();
	
	public static void start(String jdDir){
		//--- setup dirs
		rootDir = jdDir;
		srcFolder = new File(rootDir + "/cfg/");
		FilenameFilter filenamefilter = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				String lowercaseName = name.toLowerCase();
				if (lowercaseName.contains("downloadlist")) {
					return true;
				} else {
					return false;
				}
			}
		};
		
		//--- determine newest List
		File[] dlLists = srcFolder.listFiles(filenamefilter);
		Arrays.sort(dlLists, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
		for(File f : dlLists){
			if(f.getName().endsWith(".zip")){
				downloadList = f;
				break;
			}
		}
		
		try {
			zf = new ZipFile(downloadList);
			sList = new ArrayList<>();
			for(Enumeration<? extends ZipEntry> e = zf.entries(); e.hasMoreElements();){
				s = new StringBuilder();
				entry = e.nextElement();
				if(!entry.getName().contains("extraInfo")){
					is = zf.getInputStream(entry);
					isr = new InputStreamReader(is);
					buffer = new char[1024];
					int read = 0;
					while ((read = isr.read(buffer, 0, buffer.length)) >= 0) {
						s.append(new String(buffer, 0, read));
					}
					sList.add(s);
				}
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//--- parse String
		parseStringBuilder();
		
		
	}

	private static void parseStringBuilder() {
		for(StringBuilder sb : sList){
			Container c = new Container(parseContainer(sb));
			containerList.add(c);
		}
	}

	private static Container parseContainer(StringBuilder sb) {
		int startSub = sb.lastIndexOf(LINKSSNIPPED_END);
		int endSub = sb.length();
		String sub = sb.substring(startSub, endSub);
		String name = getNameFromContainerInfoSnippet(sub);
		Properties properties = null;
		long uid = getUIDFromContainerInfoSnippet(sub);
		long created = getCreatedFromContainerInfoSnippet(sub);
		String downloadFolder = getDownloadFolderFromContainerInfoSnippet(sub);
		
		endSub = startSub;
		startSub = sb.indexOf(LINKSSNIPPED_START) + LINKSSNIPPED_START_LENGTH;
		List<Link> linkList = getLinksListFromLinksSnippet(sb.substring(startSub, endSub));
		return new Container(name, properties, uid, created, downloadFolder, linkList, getUniqueNames(linkList));
	}

	private static String getDownloadFolderFromContainerInfoSnippet(String sub) {
		int startSub = sub.indexOf(DOWNLOADFOLDER) + DISTANCE_DOWNLOADFOLDER_STRING;
		int endSub = sub.substring(startSub).indexOf(QUOTE) + startSub;
		return sub.substring(startSub, endSub);
	}

	private static long getCreatedFromContainerInfoSnippet(String sub) {
		int startSub = sub.indexOf(CREATED) + DISTANCE_CREATED_STRING;
		int endSub = sub.substring(startSub).indexOf(COMMA) + startSub;
		return Long.parseLong(sub.substring(startSub, endSub));
	}

	private static long getUIDFromContainerInfoSnippet(String sub) {
		int startSub = sub.indexOf(UID) + DISTANCE_UID_STRING;
		int endSub = sub.substring(startSub).indexOf(COMMA) + startSub;
		return Long.parseLong(sub.substring(startSub, endSub));
	}

	private static List<String> getUniqueNames(List<Link> linkList) {
		Set<String> uniqueSet = new HashSet<>();
		List<String> uniqueNameList	= new ArrayList<>();
		String name;
		for(Link l : linkList){
			name = trimNamePart(l.getName());
			if(uniqueSet.add(name)){
				uniqueNameList.add(name);
			}
		}
		return uniqueNameList;
	}

	private static String getNameFromContainerInfoSnippet(String sub){
		int startSub = sub.indexOf(NAME) + DISTANCE_NAME_STRING;
		int endSub = sub.indexOf(QUOTE+COMMA) + startSub;
		return sub.substring(startSub).substring(startSub, endSub);
	}
	
	private static List<Link> getLinksListFromLinksSnippet(final String sub) {
		
		String srg = sub;
		int startSrg;
		int endSrg;
		
		List<Link> link = new ArrayList<>();
		Set<String> linksName = new HashSet<>();
//		while(srg.contains(NAME)){
			startSrg = srg.indexOf(NAME) + DISTANCE_NAME_STRING;
			endSrg = srg.indexOf(QUOTE+COMMA, startSrg);
			String name = trimNameRAR(srg.substring(startSrg, endSrg));
//			srg = srg.substring(endSrg, srg.length());
			
			startSrg = srg.indexOf(BROWSERURL) + DISTANCE_BROWSERURL_STRING;
			endSrg = srg.indexOf(COMMA, startSrg);
			String browserUrl = srg.substring(startSrg, endSrg);
			
			startSrg = srg.indexOf(HOST) + DISTANCE_HOST_STRING;
			endSrg = srg.indexOf(QUOTE+COMMA, startSrg);
			URL host = null;
			try {
				host = new URL(srg.substring(startSrg, endSrg));
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			startSrg = srg.indexOf(CREATED) + DISTANCE_CREATED_STRING;
			endSrg = srg.indexOf(COMMA, startSrg);
			long created = Long.parseLong(srg.substring(startSrg, endSrg));
			
			startSrg = srg.indexOf(SIZE) + DISTANCE_SIZE_NUMBER;
			endSrg = srg.indexOf(COMMA, startSrg);
			long size = Long.parseLong(srg.substring(startSrg, endSrg));
			
			startSrg = srg.indexOf(UID) + DISTANCE_UID_STRING;
			endSrg = srg.indexOf(COMMA, startSrg);
			long uid = Long.parseLong(srg.substring(startSrg, endSrg));
			
			startSrg = srg.indexOf(URL) + DISTANCE_URL_STRING;
			endSrg = srg.indexOf(COMMA, startSrg);
			long url = Long.parseLong(srg.substring(startSrg, endSrg));
			
//			if(srg.contains(SIZE)){
//				startSrg = srg.indexOf(SIZE) + DISTANCE_SIZE_NUMBER;
//				endSrg = srg.indexOf(COMMA, startSrg);
//				String sizeString = srg.substring(startSrg, endSrg);
//				try{
//					size = Long.parseLong(sizeString);
//				}catch(NumberFormatException e){
//					System.out.println("NumberFormatException while parsing the following String: " + srg.substring(startSrg, endSrg));
//				}
//				srg = sub.substring(endSrg, sub.length());
//			}
			
			if(linksName.add(name)){
				link.add(new Link(name, size, host, uid, created, browserUrl));
			}
//		}
		return link;
	}

	private static String trimNameRAR(String s) {
//		if(s.contains(".part")){
//			return s.substring(0, s.indexOf(".part"));
//		}else
		if(s.contains(".rar")){
			return s.substring(0, s.indexOf(".rar"));
		}else{
			return s;
		}
	}
	private static String trimNamePart(String s){
		if(s.contains(".part")){
			return s.substring(0, s.indexOf(".part"));
		}else{
			return s;
		}
	}
	
	private static long getTotalSize(Link link){
		return -1;
	}
}
