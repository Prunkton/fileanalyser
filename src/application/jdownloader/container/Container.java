package application.jdownloader.container;

import java.util.List;

public class Container {
	private List<Link> link;
	private String name;
	private Properties properties;
	private long created;
	private long uid;
	private String downloadFolder;
	
	//--- custom Objects
	private List<String> uniqueNames;
	
	public Container(Container c){
		this.link = c.link;
		this.name = c.name;
	}
	public Container(String name, Properties properties, long uid, long created, String downloadFolder, List<Link> link, List<String> uniqueNames){
		this.name = name;
		this.properties = properties;
		this.uid = uid;
		this.created = created;
		this.downloadFolder = downloadFolder;
		this.link = link;
	}
	
	public String getName(){
		return this.name;
	}
	
	public List<Link> getLinks(){
		return this.link;
	}
}
