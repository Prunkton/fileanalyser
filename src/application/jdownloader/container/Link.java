package application.jdownloader.container;

import java.net.URL;

public class Link {
	private String name;
	private URL url;
	private String properties;
	private long size;
	private URL host;
	private boolean enabled;
	private long created;
	private Object linkStatus;
	private long uid;
	private URL browserURL;
	private int linkType;
	private int current;
	private Object finalLinkState;
	private boolean availablestatus;
	private Object chunkProgress;
	private String propertiesString;
	
	public Link(String name, long size, URL host, long uid,
			long created, String browserUrl) {

		this.name = name;
		this.size = size;
		this.host = host;
		this.uid = uid;
		this.created = created;
		this.browserURL = browserURL;
	}

	public String getName(){
		return name;
	}
	
	/**
	 * todo: filter file extentions & other meta information
	 * @return
	 */
	public String getNameFiltered(){
		return name;
	}
	
	public long getSize(){
		return size;
	}
}
