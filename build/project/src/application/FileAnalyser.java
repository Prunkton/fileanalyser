package application;

import java.io.File;
import java.io.FileFilter;
import java.io.RandomAccessFile;
import java.util.*;

import org.apache.commons.io.filefilter.HiddenFileFilter;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBoxBuilder;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import com.xuggle.xuggler.IContainer;
import com.xuggle.xuggler.IContainerFormat;
import com.xuggle.xuggler.IStream;
import com.xuggle.xuggler.IStreamCoder;

public class FileAnalyser {
	
	private enum Response{YES, NO, CANCEL};
	private static Response buttonSelected = Response.CANCEL;
	
	private static boolean isDubNameOn = true;
	private static boolean isDubHashOn = true;
	private static boolean isDubSizeOn = true;
	private static boolean isDubDurationOn = true;
	
	private static List<FileInformation> fiListAll = new ArrayList<>();
	private static List<DublicateFileInfo> dfiList = new ArrayList<>();
	private static List<File> folderList = new ArrayList<>();
	private static RandomAccessFile raf;
	
	static void addFile(File f){
//		MessageDigest md;
//		try {
//			md = MessageDigest.getInstance("MD5");
//			try (InputStream is = Files.newInputStream(Paths.get(f.getAbsolutePath()))) {
//				DigestInputStream dis = new DigestInputStream(is, md);
				/* Read stream to EOF as normal... */
				try{
//					System.out.println(f.getName());
//					IContainer container = IContainer.make();
//					IContainerFormat contInpFormat = IContainerFormat.make();
//					if (container.open(f.getAbsolutePath(), IContainer.Type.READ, contInpFormat) < 0) {
//						return;
////						throw new RuntimeException("Cannot open '" + f.getAbsolutePath() + "'");
//					}
//					IStream stream = container.getStream(0);
//					// Get the pre-configured decoder that can decode this stream;
//					IStreamCoder coder = stream.getStreamCoder();
					byte[] b = {1};
					raf = new RandomAccessFile(f, "r");
					fiListAll.add(new FileInformation(f, b /*md.digest()*/, raf.length(), null, null));//container, coder));
					raf.close();
				}catch(Throwable e){
					e.printStackTrace();
				}
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		} catch (NoSuchAlgorithmException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		ht.put(f, new FileInformation(f, digest, f.length()));
	}
	
	private static void resetLists(){
		fiListAll = new ArrayList<>();
		dfiList = new ArrayList<>();
		folderList = new ArrayList<>();
	}
	
	public static void scanFolders(List<File> folderList0){
		resetLists();
		folderList = folderList0;
		for(File f : folderList){
			scanSubFolder(f);
		}
		findDublicates();
	}
	
	private static void scanSubFolder(File f){
		for(int i = 0; i < f.listFiles((FileFilter)HiddenFileFilter.VISIBLE).length; i++){
			if(f.listFiles((FileFilter)HiddenFileFilter.VISIBLE)[i].isDirectory()){
				scanSubFolder(f.listFiles((FileFilter)HiddenFileFilter.VISIBLE)[i]);
			}else{
				addFile(f.listFiles((FileFilter)HiddenFileFilter.VISIBLE)[i]);
			}
		}
	}
	
	private static void findDublicates(){
		
		DublicateFileInfo dfiTmp = null;
		Map<String, Boolean> uniqNames = new HashMap<>();
		Map<String, Boolean> uniqHash = new HashMap<>();
		Map<Long, Boolean> uniqSize = new HashMap<>();
		Map<Long, Boolean> uniqDuration = new HashMap<>();
		
//		testMap.c
		for(FileInformation f : fiListAll){
			
			if(isDubNameOn){
				if(uniqNames.containsKey(f.getName())){				//same filename already found
					if(uniqNames.get(f.getName())){					//first found filename already in DublicateFileInfo List
						FINDDUBNAHME : for(DublicateFileInfo dfi : dfiList){		//find the DublicateFileInfo Element
							for(FileInformation fi : dfi.getFileInfos()){
								if(fi.getName().equals(f.getName())){
									dfi.addFileInfo(f);					//add the new found FileInformation into the dfi list
									dfi.setDubName();
									break FINDDUBNAHME;
								}
							}
						}
					}else{
						dfiTmp = new DublicateFileInfo(f);
						for(FileInformation fi : fiListAll){
							if(fi.getName().equals(f.getName())){
								dfiTmp.addFileInfo(fi);
								break;
							}
						}
						dfiTmp.addFileInfo(f);
						dfiTmp.setDubName();
						uniqNames.put(f.getName(), true);
						dfiList.add(dfiTmp);
						dfiTmp = null;
					}
				}else{
					uniqNames.put(f.getName(), new Boolean(false));
				}
			}
//			if(isDubHashOn){
//				if(uniqHash.containsKey(f.getHash())){				//same filename already found
//					if(uniqHash.get(f.getHash())){					//first found filename already in DublicateFileInfo List
//						FINDDUBHASH : for(DublicateFileInfo dfi : dfiList){		//find the DublicateFileInfo Element
//							for(FileInformation fi : dfi.getFileInfos()){
//								if(fi.getHash().equals(f.getHash())){
//									dfi.addFileInfo(f);					//add the new found FileInformation into the dfi list
//									dfi.setDubHash();
//									break FINDDUBHASH;
//								}
//							}
//						}
//					}else{
//						dfiTmp = new DublicateFileInfo(f);
//						for(FileInformation fi : fiListAll){
//							if(fi.getHash().equals(f.getHash())){
//								dfiTmp.addFileInfo(fi);
//								break;
//							}
//						}
//						dfiTmp.addFileInfo(f);
//						dfiTmp.setDubHash();
//						uniqHash.put(f.getHash(), true);
//						dfiList.add(dfiTmp);
//						dfiTmp = null;
//					}
//				}else{
//					uniqHash.put(f.getHash(), new Boolean(false));
//				}
//			}
			if(isDubSizeOn){
				if(uniqSize.containsKey(f.getSize())){				//same filename already found
					if(uniqSize.get(f.getSize())){					//first found filename already in DublicateFileInfo List
						FINDDUBSIZE : for(DublicateFileInfo dfi : dfiList){		//find the DublicateFileInfo Element
							for(FileInformation fi : dfi.getFileInfos()){
								if(fi.getSize().equals(f.getSize())){
									dfi.addFileInfo(f);					//add the new found FileInformation into the dfi list
									dfi.setDubSize();
									break FINDDUBSIZE;
								}
							}
						}
					}else{
						boolean found = false;
						FINDDUBNAHME : for(DublicateFileInfo dfi : dfiList){		//find the DublicateFileInfo Element
							for(FileInformation fi : dfi.getFileInfos()){
								if(fi.getSize().equals(f.getSize())){
//									dfi.addFileInfo(f);					//add the new found FileInformation into the dfi list
									dfi.setDubSize();
									found = true;
									break FINDDUBNAHME;
								}
							}
						}
						if(!found){
							dfiTmp = new DublicateFileInfo(f);
							for(FileInformation fi : fiListAll){
								if(fi.getSize().equals(f.getSize())){
									dfiTmp.addFileInfo(fi);
									break;
								}
							}
							dfiTmp.addFileInfo(f);
							dfiTmp.setDubSize();
							uniqSize.put(f.getSize(), true);
							dfiList.add(dfiTmp);
							dfiTmp = null;
						}
					}
				}else{
					uniqSize.put(f.getSize(), new Boolean(false));
				}
			}
//			if(isDubDurationOn){
//				if(uniqDuration.containsKey(f.getDuration())){				//same filename already found
//					if(uniqDuration.get(f.getDuration())){					//first found filename already in DublicateFileInfo List
//						FINDDUBDURATION : for(DublicateFileInfo dfi : dfiList){		//find the DublicateFileInfo Element
//							for(FileInformation fi : dfi.getFileInfos()){
//								if(fi.getDuration().equals(f.getDuration())){
//									dfi.addFileInfo(f);					//add the new found FileInformation into the dfi list
//									dfi.setDubDuration();
//									break FINDDUBDURATION;
//								}
//							}
//						}
//					}else{
//						boolean found = false;
//						FINDDUBNAHME : for(DublicateFileInfo dfi : dfiList){		//find the DublicateFileInfo Element
//							for(FileInformation fi : dfi.getFileInfos()){
//								if(fi.getDuration().equals(f.getDuration())){
////									dfi.addFileInfo(f);					//add the new found FileInformation into the dfi list
//									dfi.setDubDuration();
//									found = true;
//									break FINDDUBNAHME;
//								}
//							}
//						}
//						if(!found){
//							dfiTmp = new DublicateFileInfo(f);
//							for(FileInformation fi : fiListAll){
//								if(fi.getDuration().equals(f.getDuration())){
//									dfiTmp.addFileInfo(fi);
//									break;
//								}
//							}
//							dfiTmp.addFileInfo(f);
//							dfiTmp.setDubDuration();
//							uniqDuration.put(f.getDuration(), true);
//							dfiList.add(dfiTmp);
//							dfiTmp = null;
//						}
//					}
//				}else{
//					uniqDuration.put(f.getDuration(), new Boolean(false));
//				}
//			}
		}
	}
	
	public static boolean deleteFileFromDisc(File f){
		final Stage dialogStage = new Stage();
		dialogStage.initModality(Modality.WINDOW_MODAL);
		Button btnOk = new Button("Ok");
		Button btnCanel = new Button("Cancel");
		btnOk.setOnAction( new EventHandler<ActionEvent>() {
			@Override public void handle( ActionEvent e ) {
				dialogStage.close();
				buttonSelected = Response.YES;
			}
		});
		btnCanel.setOnAction( new EventHandler<ActionEvent>() {
			@Override public void handle( ActionEvent e ) {
				dialogStage.close();
				buttonSelected = Response.CANCEL;
			}
		});
		dialogStage.setScene(new Scene(HBoxBuilder.create().
		    children(new Text("Do you realy want to delete the file " + f.getName()), btnOk, btnCanel).
		    alignment(Pos.CENTER).padding(new Insets(5)).build()));
		dialogStage.showAndWait();
		
		if(buttonSelected == Response.YES){
			try{
				return f.delete();
			}catch(Exception e){
				e.printStackTrace();
				return false;
			}
		}else{
			return false;
		}
		
//		return buttonSelected == Response.YES;
	}
//	
//	private boolean deleteFileAction(File f){
//		
//	}
	public static void setIsDubNameOn(boolean isOn){
		isDubNameOn = isOn;
	}
	public static void setIsDubHashOn(boolean isOn){
		isDubHashOn = isOn;
	}
	public static void setIsDubSizeOn(boolean isOn){
		isDubSizeOn = isOn;
	}
	public static void setIsDubDurationOn(boolean isOn){
		isDubDurationOn = isOn;
	}
	
	public static void addFolder2List(File f){
		folderList.add(f);
	}
	public static void removeFolderFromList(File f){
		folderList.remove(f);
	}
	
	public static List<DublicateFileInfo> getDublicates(){
		return dfiList;
	}
	
//	public static List<FileInformation> getFileInfoDubs(){
//		return fiListDubs;
//	}

}
