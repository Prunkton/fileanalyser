package application;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

import com.xuggle.xuggler.IContainer;
import com.xuggle.xuggler.IStreamCoder;

public class DublicateFileInfo {
	private final SimpleStringProperty name;
	private final List<FileInformation> fiList = new ArrayList<>();
//	private SimpleIntegerProperty count = (fiList == null) ? new SimpleIntegerProperty(0) : new SimpleIntegerProperty(fiList.size());
	
	private boolean isDubName;
	private boolean isDubHash;
	private boolean isDubSize;
	private boolean isDubDuration;

	
	public DublicateFileInfo(File f, byte[] hash, IContainer container, IStreamCoder coder){
		this.name = new SimpleStringProperty(f.getName().substring(0, f.getName().lastIndexOf(".")));
	}
	
	public DublicateFileInfo(FileInformation fi){
		this(fi.getFile(), fi.getHashByte(), fi.getContainer(), fi.getCoder());
	}
	
	public void addFileInfo(FileInformation fi){
		fiList.add(fi);
	}
	public List<FileInformation> getFileInfos(){
		return fiList;
	}
	
	public String getName() {
		return name.get();
	}

	public void setName(String fName) {
		name.set(fName);
	}

	public Integer getCount(){
		return fiList.size();
	}
	
	public void setDubName(){
		isDubName = true;
	}
	public void setDubHash(){
		isDubHash = true;
	}
	public void setDubSize(){
		isDubSize = true;
	}
	public void setDubDuration(){
		isDubDuration = true;
	}
	
	public String getIsDubName(){
		return isDubName ? "x" : "";
	}
	public String getIsDubHash(){
		return isDubHash ? "x" : "";
	}
	public String getIsDubSize(){
		return isDubSize ? "x" : "";
	}
	public String getIsDubDuration(){
		return isDubDuration ? "x" : "";
	}
	
	@Override
	public String toString(){
		return this.getName();
	}
}
