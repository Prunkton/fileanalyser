package application.jdownloader.container;

import java.util.List;

public class Container {
	private List<Links> links;
	private String name;
	private Properties properties;
	private boolean created;
	private long uid;
	private String downloadFolder;
	
	//--- custom Objects
	private List<String> uniqueNames;
	
	public Container(Container c){
		this.links = c.links;
		this.name = c.name;
	}
	public Container(String name, List<Links> links, List<String> uniqueNames){
		this.name = name;
		this.links = links;
	}
}
