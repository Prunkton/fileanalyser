package application.jdownloader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import org.apache.commons.io.comparator.LastModifiedFileComparator;

import application.jdownloader.container.Container;
import application.jdownloader.container.Links;

import com.github.junrar.Archive;
import com.github.junrar.exception.RarException;
import com.github.junrar.impl.FileVolumeManager;
import com.github.junrar.rarfile.FileHeader;

public class JDReader {
	
	private static final String LINKSSNIPPED_START = " : [ {";
	private static final String LINKSSNIPPED_END = "  } ],";
	private static final String NAME = "name";
	private static final String SIZE = "size";
	private static final String COMMA = ",";
	private static final String QUOTE = "\"";
	private static final String COLON = ":";
	
	private static final int LINKSSNIPPED_START_LENGTH = 6;
	private static final int DISTANCE_SIZE_NUMBER = 4+4; //name + extension
	private static final int DISTANCE_NAME_STRING = 4+5; //name + extension
	
	private static String rootDir;
	private static File srcFolder;
	private static File downloadList;
	
	private static int refreshTime = 10000;
	
	private static InputStream is;
	private static InputStreamReader isr;
	private static ZipFile zf;;
	private static ZipEntry entry;
	private static StringBuilder s;
	private static List<StringBuilder> sList;
	private static char[] buffer;
	
	private static List<Container> containerList = new ArrayList<>();
	
	public static void start(String jdDir){
		//--- setup dirs
		rootDir = jdDir;
		srcFolder = new File(rootDir + "/cfg/");
		FilenameFilter filenamefilter = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				String lowercaseName = name.toLowerCase();
				if (lowercaseName.contains("downloadlist")) {
					return true;
				} else {
					return false;
				}
			}
		};
		
		//--- determine newest List
		File[] dlLists = srcFolder.listFiles(filenamefilter);
		Arrays.sort(dlLists, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
		for(File f : dlLists){
			if(f.getName().endsWith(".zip")){
				downloadList = f;
				break;
			}
		}
		
		try {
			zf = new ZipFile(downloadList);
			sList = new ArrayList<>();
			for(Enumeration<? extends ZipEntry> e = zf.entries(); e.hasMoreElements();){
				s = new StringBuilder();
				entry = e.nextElement();
				if(!entry.getName().contains("extraInfo")){
					is = zf.getInputStream(entry);
					isr = new InputStreamReader(is);
					buffer = new char[1024];
					int read = 0;
					while ((read = isr.read(buffer, 0, buffer.length)) >= 0) {
						s.append(new String(buffer, 0, read));
					}
					sList.add(s);
				}
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//--- parse String
		parseStringBuilder();
	}

	private static void parseStringBuilder() {
		for(StringBuilder sb : sList){
			Container c = new Container(parseContainer(sb));
			containerList.add(c);
		}
	}

	private static Container parseContainer(StringBuilder sb) {
		int startSub = sb.lastIndexOf(LINKSSNIPPED_END);
		int endSub = sb.length();
		String sub = sb.substring(startSub, endSub);
		String name = getNameFromContainerInfoSnippet(sub);
		
		endSub = startSub;
		startSub = sb.indexOf(LINKSSNIPPED_START) + LINKSSNIPPED_START_LENGTH;
		List<Links> linkList = getLinksListFromLinksSnippet(sb.substring(startSub, endSub));
		return new Container(name, linkList, getUniqueNames(linkList));
	}

	private static List<String> getUniqueNames(List<Links> linkList) {
		Set<String> uniqueSet = new HashSet<>();
		List<String> uniqueNameList	= new ArrayList<>();
		String name;
		for(Links l : linkList){
			name = trimNamePart(l.getName());
			if(uniqueSet.add(name)){
				uniqueNameList.add(name);
			}
		}
		return uniqueNameList;
	}

	private static String getNameFromContainerInfoSnippet(String sub){
		int startSub = sub.indexOf(NAME) + DISTANCE_NAME_STRING;
		int endSub = sub.indexOf(QUOTE+COMMA);
		return sub.substring(startSub, endSub);
	}
	
	private static List<Links> getLinksListFromLinksSnippet(String sub) {
		int startSub;
		int endSub;
		String name;
		long size = -1;
		List<Links> links = new ArrayList<>();
		Set<String> linksName = new HashSet<>();
		while(sub.contains(NAME)){
			startSub = sub.indexOf(NAME) + DISTANCE_NAME_STRING;
			endSub = sub.indexOf(QUOTE+COMMA, startSub);
			name = sub.substring(startSub, endSub);
			name = trimNameRAR(name);
			sub = sub.substring(endSub, sub.length());
			
			if(sub.contains(SIZE)){
				startSub = sub.indexOf(SIZE) + DISTANCE_SIZE_NUMBER;
				endSub = sub.indexOf(COMMA, startSub);
				String sizeString = sub.substring(startSub, endSub);
				try{
					size = Long.parseLong(sizeString);
				}catch(NumberFormatException e){
					System.out.println("NumberFormatException while parsing the following String: " + sub.substring(startSub, endSub));
				}
				sub = sub.substring(endSub, sub.length());
			}
			
			if(linksName.add(name)){
				links.add(new Links(name, size));
			}
		}
		return links;
	}

	private static String trimNameRAR(String s) {
//		if(s.contains(".part")){
//			return s.substring(0, s.indexOf(".part"));
//		}else
		if(s.contains(".rar")){
			return s.substring(0, s.indexOf(".rar"));
		}else{
			return s;
		}
	}
	private static String trimNamePart(String s){
		if(s.contains(".part")){
			return s.substring(0, s.indexOf(".part"));
		}else{
			return s;
		}
	}
	
	private static long getTotalSize(Links link){
		return -1;
	}
}
