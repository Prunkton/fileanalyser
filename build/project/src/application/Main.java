package application;
	
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.fxml.FXMLLoader;


public class Main extends Application {
	
	static Stage primaryStage;
	
	@Override
	public void start(Stage primaryStage0) {
		try {
			primaryStage = primaryStage0;
			primaryStage.setTitle("FileAnalyser 1.0");
			AnchorPane root = (AnchorPane)FXMLLoader.load(getClass().getResource("MainUI.fxml"));
			Scene scene = new Scene(root,1000,600);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
	public static Stage getPrimaryStage(){
		return primaryStage;
	}
}
