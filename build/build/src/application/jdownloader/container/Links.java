package application.jdownloader.container;

import java.net.URL;

public class Links {
	private String name;
	private URL url;
	private String properties;
	private long size;
	private URL host;
	private boolean enabled;
	private long created;
	private Object linkStatus;
	private long uid;
	private URL browserURL;
	private int linkType;
	private int current;
	private Object finalLinkState;
	private boolean availablestatus;
	private Object chunkProgress;
	private String propertiesString;
	
	public Links(String name, long size){
		this.name = name;
		this.size = size;
	}
	
	public String getName(){
		return name;
	}
}
