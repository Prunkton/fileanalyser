package application.jdownloader.container;

import java.net.URL;

public class DownloadLink {
	private String name;
	private URL url;
	private Object properties;
	private long size;
	private URL host;
	private long uid;
	private boolean enabled;
	private long created;
	private Object linkStatus;
	private Object finalLinkState;
	private int linkType;
	private int current;
	private boolean availablestatus;
	private Object chunkProgress;
	private String propertiesString;
	private URL browerURL;
}
