package application;

import java.io.File;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Calendar;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.xuggle.xuggler.IContainer;
import com.xuggle.xuggler.IStreamCoder;

//@PersistenceCapable
//@Entity
public class FileInformation {
//	@Id
	private final SimpleStringProperty name;
	private final SimpleStringProperty path;
	private final SimpleStringProperty type;
	private final SimpleLongProperty size;
//	private final SimpleIntegerProperty bitrate;
//	private final SimpleStringProperty hashString;
//	private final SimpleLongProperty duration;
//	private final SimpleObjectProperty<Calendar> dateCreation;
//	private final SimpleObjectProperty<Calendar> dateLastEdit;

	private File f;
	private byte[] hashByte;
	private IContainer container;
	private IStreamCoder coder;
	
	public FileInformation(File f, byte[] hash, long size, IContainer container, IStreamCoder coder){
//		if(container != null ){
//			this.size = (container.getFileSize() == 0) ? new SimpleLongProperty(f.length()) : new SimpleLongProperty(container.getFileSize());
//		}else{
//			this.size = new SimpleLongProperty(-1);
//		}
		this.size = new SimpleLongProperty(size);
		this.path = new SimpleStringProperty(f.getAbsolutePath());
		this.name = new SimpleStringProperty(f.getName().substring(0, f.getName().lastIndexOf(".")));
		this.type = new SimpleStringProperty(f.getName().substring(f.getName().lastIndexOf("."), f.getName().length()));
//		this.bitrate = (coder == null) ? new SimpleIntegerProperty(0) : new SimpleIntegerProperty(coder.getBitRate());
//		this.hashString = new SimpleStringProperty(new BigInteger(1, hash).toString(16));
//		this.duration = new SimpleLongProperty(container.getDuration());
		
		this.f = f;
		this.hashByte = hash;
		this.container = container;
		this.coder = coder;
	}
	
	public String getName() {
		return name.get();
	}
	
	public String getPath(){
		return path.get();
	}
	
	public String getType(){
		return type.get();
	}
	
	public Long getSize(){
		return size.get();
	}
	
//	public Integer getBitrate(){
//		return bitrate.get();
//	}
//	
//	public String getHash(){
//		return hashString.get();
//	}
//	
//	public Long getDuration(){
//		return duration.get();
//	}
	
	
	public File getFile(){
		return f;
	}
	
	public byte[] getHashByte(){
		return hashByte;
	}
	
	public IContainer getContainer(){
		return container;
	}
	
	public IStreamCoder getCoder(){
		return coder;
	}
	
	@Override
	public String toString(){
		return "Name: " + this.getName() + ", Size: " + this.getSize();
	}
}
